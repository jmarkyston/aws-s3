﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using MTJ.AWS.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MTJ.AWS.S3
{
    public class AwsS3Client : AwsClientBase<IAmazonS3>
    {
        private Dictionary<MimeType, string> MimeTypeMap = new Dictionary<MimeType, string>()
        {
            { MimeType.VideoMp4, "video/mp4" },
            { MimeType.ImageJpeg, "image/jpeg" }
        };

        private Dictionary<string, MimeType> ExtensionMimeTypeMap = new Dictionary<string, MimeType>()
        {
            { "mp4", MimeType.VideoMp4 },
            { "jpg", MimeType.ImageJpeg }
        };

        public enum MimeType
        {
            VideoMp4,
            ImageJpeg
        }

        public AwsS3Client(): base() { }
        public AwsS3Client(string accessKey, string secretKey, string region): base(accessKey, secretKey, region) { }

        private async Task PutFile(string bucketName, string key, object content, bool isPublic = false, MimeType? contentType = null, Dictionary<string, string> metadata = null)
        {
            var request = new PutObjectRequest()
            {
                BucketName = bucketName,
                Key = key,
                StorageClass = S3StorageClass.StandardInfrequentAccess
            };
            if (content is string)
                request.FilePath = (string)content;
            else
                request.InputStream = (Stream)content;
            if (isPublic)
                request.CannedACL = S3CannedACL.PublicRead;
            if (contentType.HasValue)
                request.ContentType = MimeTypeMap[contentType.Value];
            if (metadata != null && metadata.Count > 0)
            {
                foreach (KeyValuePair<string, string> pair in metadata)
                {
                    string metaPre = "x-amz-meta-";
                    string pre = pair.Key.StartsWith(metaPre) ? string.Empty : metaPre;
                    request.Metadata.Add($"{pre}{pair.Key.ToLower()}", pair.Value);
                }
            }
            await ServiceClient.PutObjectAsync(request);
        }
        public async Task PutFile(string bucketName, string key, string filePath, bool isPublic = false, Dictionary<string, string> metadata = null)
        {
            string extension = filePath.Substring(filePath.LastIndexOf('.') + 1);
            MimeType? type = ExtensionMimeTypeMap.Keys.Contains(extension)
                ? (MimeType?)ExtensionMimeTypeMap[extension]
                : null;
            await PutFile(bucketName, key, filePath, isPublic, type, metadata);
        }
        public async Task PutFile(string bucketName, string key, Stream stream, MimeType contentType, bool isPublic = false, Dictionary<string, string> metadata = null)
        {
            await PutFile(bucketName, key, stream, isPublic, contentType, metadata);
        }

        public async Task DeleteObject(string bucketName, string key)
        {
            await ServiceClient.DeleteObjectAsync(bucketName, key);
        }

        public async Task<S3Object[]> ListObjects(string bucketName, string prefix = null, int maxKeys = 1000)
        {
            var request = new ListObjectsV2Request()
            {
                BucketName = bucketName,
                Prefix = prefix,
                MaxKeys = maxKeys
            };
            ListObjectsV2Response response = await ServiceClient.ListObjectsV2Async(request);
            IEnumerable<S3Object> objects = response.S3Objects;
            if (prefix != null)
                objects = objects.Where(o => o.Key != $"{prefix}/");
            return objects.ToArray();
        }
        public async Task<S3Object[]> ListObjects(string bucketName, int maxKeys = 1000)
        {
            return await ListObjects(bucketName, null, maxKeys);
        }

        public string ComposeUrl(string bucketName, string key)
        {
            return $"https://s3.{Region}.amazonaws.com/{bucketName}/{key}";
        }
    }
}
